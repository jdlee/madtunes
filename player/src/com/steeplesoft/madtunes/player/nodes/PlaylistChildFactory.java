/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.steeplesoft.madtunes.player.nodes;

import com.steeplesoft.madsonic.restclient.SubsonicClient;
import com.steeplesoft.madsonic.restclient.model.Playlist;
import java.beans.IntrospectionException;
import java.util.List;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

/**
 *
 * @author jdlee
 */
public class PlaylistChildFactory extends ChildFactory<Playlist> {
    @Override
    protected boolean createKeys(List<Playlist> toPopulate) {
        toPopulate.addAll(SubsonicClient.getInstance().getPlaylists());
        
        return true;
    }

    @Override
    protected Node createNodeForKey(Playlist key) {
       PlaylistNode node = null;
       
        try {
            node = new PlaylistNode(key);
        } catch (IntrospectionException ex) {
            Exceptions.printStackTrace(ex);
        }
       
       return node;
    }
    
}
