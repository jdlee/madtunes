/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.steeplesoft.madtunes.player.nodes;

import com.steeplesoft.madsonic.restclient.model.Album;
import java.beans.IntrospectionException;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author jdlee
 */
public class AlbumNode extends AbstractNode {
    private Album album;

    public AlbumNode(Album bean) throws IntrospectionException {
        super(Children.LEAF, Lookups.singleton(bean));
        setDisplayName(bean.getName());
    }

    public Album getAlbum() {
        return album;
    }
    
    
}
