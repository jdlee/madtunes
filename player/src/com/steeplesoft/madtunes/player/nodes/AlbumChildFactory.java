/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.madtunes.player.nodes;

import com.steeplesoft.madsonic.restclient.SubsonicClient;
import com.steeplesoft.madsonic.restclient.model.Album;
import com.steeplesoft.madsonic.restclient.model.Artist;
import java.beans.IntrospectionException;
import java.util.List;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

/**
 *
 * @author jdlee
 */
public class AlbumChildFactory extends ChildFactory<Album> {

    private final Artist artist;
    private final SubsonicClient client;

    public AlbumChildFactory(Artist artist) {
        this.artist = artist;
        client = SubsonicClient.getInstance();
    }

    @Override
    protected boolean createKeys(List<Album> toPopulate) {
        toPopulate.addAll(client.getAlbums(artist));
        return true;
    }

    @Override
    protected Node createNodeForKey(Album key) {
        AlbumNode node = null;

        try {
            node = new AlbumNode(key);
        } catch (IntrospectionException ex) {
            Exceptions.printStackTrace(ex);
        }

        return node;
    }

}
