/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.steeplesoft.madtunes.player.nodes;

import com.steeplesoft.madsonic.restclient.model.Artist;
import java.beans.IntrospectionException;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author jdlee
 */
public class ArtistNode extends AbstractNode {
    private Artist artist;

    public ArtistNode(Artist bean) throws IntrospectionException {
        super(Children.create(new AlbumChildFactory(bean), true), Lookups.singleton(bean));
        setDisplayName(bean.getName());
    }

    public Artist getArtist() {
        return artist;
    }
}
