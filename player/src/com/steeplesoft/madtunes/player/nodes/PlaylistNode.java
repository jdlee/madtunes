/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.steeplesoft.madtunes.player.nodes;

import com.steeplesoft.madsonic.restclient.model.Playlist;
import java.beans.IntrospectionException;
import org.openide.nodes.BeanNode;
import org.openide.nodes.Children;

/**
 *
 * @author jdlee
 */
public class PlaylistNode extends BeanNode<Playlist> {
    public PlaylistNode(Playlist bean) throws IntrospectionException {
        super(bean, Children.LEAF);
        setDisplayName(bean.getName());
    }
    
}
