/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.madtunes.player.nodes;

import com.steeplesoft.madsonic.restclient.SubsonicClient;
import com.steeplesoft.madsonic.restclient.model.Artist;
import java.beans.IntrospectionException;
import java.beans.PropertyChangeEvent;
import java.util.List;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeReorderEvent;
import org.openide.util.Exceptions;

/**
 *
 * @author jdlee
 */
public class ArtistChildFactory extends ChildFactory<Artist> {

    private final SubsonicClient client;

    public ArtistChildFactory() {
        client = SubsonicClient.getInstance();
    }

    @Override
    protected boolean createKeys(List<Artist> toPopulate) {
        toPopulate.addAll(client.getArtists());

        return true;
    }

    @Override
    protected Node createNodeForKey(Artist key) {
        ArtistNode node = null;

        try {
            node = new ArtistNode(key);
        } catch (IntrospectionException ex) {
            Exceptions.printStackTrace(ex);
        }

        return node;
    }

    private static class NodeListenerImpl implements NodeListener {

        public NodeListenerImpl() {
        }

        @Override
        public void childrenAdded(NodeMemberEvent ev) {
        }

        @Override
        public void childrenRemoved(NodeMemberEvent ev) {
        }

        @Override
        public void childrenReordered(NodeReorderEvent ev) {
        }

        @Override
        public void nodeDestroyed(NodeEvent ev) {
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            System.out.println(evt.getPropertyName());
        }
    }
}
