package com.steeplesoft.madsonic.restclient;

import com.steeplesoft.madsonic.restclient.model.Album;
import com.steeplesoft.madsonic.restclient.model.Artist;
import com.steeplesoft.madsonic.restclient.model.LazyModel;
import com.steeplesoft.madsonic.restclient.model.Playlist;
import com.steeplesoft.madsonic.restclient.model.Song;
import java.net.URI;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openide.util.NbPreferences;

/**
 *
 * @author jdlee
 */
public class SubsonicClient {

    private static SubsonicClient INSTANCE;
    private final String url;
    private final String user;
    private final String pass;
    private final String version = "1.11.2";
    private Client client;
    private boolean fetchedArtists = false;
    private boolean fetchedPlaylists = false;
    private final Map<String, Artist> artistCache = new HashMap<>();
    private final Map<String, Album> albumCache = new HashMap<>();
    private final Map<String, Playlist> playlistCache = new HashMap<>();

    public static void main(String[] args) {
        SubsonicClient client = new SubsonicClient("http://bernard", "testuser", "!for testing purposes only!");
        List<Playlist> pl = client.getPlaylists();
        System.out.println(pl.toString());
    }

    public static SubsonicClient getInstance() {
        if (INSTANCE == null) {
            final Preferences prefs = NbPreferences.root();
            String server = prefs.get("server", "http://music");
            String user = prefs.get("user", "testuser");
            String password = prefs.get("pass", "!for testing purposes only!");
            INSTANCE = new SubsonicClient(server, user, password);
        }

        return INSTANCE;
    }

    protected SubsonicClient(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.pass = password;

        try {
            SSLContext ctx = SSLContext.getInstance("SSL");
            ctx.init(new KeyManager[0], new TrustManager[]{new DefaultTrustManager()}, new SecureRandom());
            SSLContext.setDefault(ctx);
            client = ClientBuilder.newBuilder()
                    .hostnameVerifier(new NoopHostnameVerifier())
                    .sslContext(ctx)
                    .build();
        } catch (NoSuchAlgorithmException | KeyManagementException ex) {
            Logger.getLogger(SubsonicClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Artist> getArtists() {
        if (!fetchedArtists) {
            fetchAllArtists();
            fetchedArtists = true;
        }
        final List<Artist> artists = new LinkedList<>(artistCache.values());
        Collections.sort(artists);
        return artists;
    }

    public Artist getArtist(String id) {
        Artist artist = artistCache.get(id);
        if (needsFetching(artist)) {
            artist = fetchArtist(id);
        }

        return artist;
    }

    public List<Album> getAlbums(Artist artist) {
        return getAlbums(artist.getId());
    }

    public List<Album> getAlbums(final String artistId) {
        List<Album> albums = null;
        Artist artist = getArtist(artistId);
        if (artist != null) {
            albums = artist.getAlbums();
        }
        return albums;
    }

    public Album getAlbum(Album album) {
        return getAlbum(album.getId());
    }

    public Album getAlbum(String id) {
        Album album = albumCache.get(id);
        if ((album == null) || (!album.hasBeenFetched())) {
            try {
                JSONObject json = getSubsonicResponse(buildRequestUri("getAlbum")
                        .queryParam("id", id));
                album = json2Album(json.getJSONObject("album"));

                JSONArray array = json.getJSONObject("album").optJSONArray("song");
                if (array != null) {
                    List<Song> songs = new LinkedList<>();
                    for (int i = 0; i < array.length(); i++) {
                        songs.add(json2Song(array.getJSONObject(i)));
                    }
                    album.setSongs(songs);
                } else {
                    JSONObject obj = json.getJSONObject("album").optJSONObject("song");
                    if (obj != null) {
                        List<Song> songs = new LinkedList<>();
                        songs.add(json2Song(obj));
                        album.setSongs(songs);
                    } else {
                        throw new RuntimeException("Song object found for album " + id);
                    }
                }
                album.setFetched();
                albumCache.put(id, album);
            } catch (JSONException ex) {
                Logger.getLogger(SubsonicClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return album;
    }

    public List<Playlist> getPlaylists() {
        if (!fetchedPlaylists) {
            fetchAllPlaylists();
            fetchedPlaylists = true;
        }
        final List<Playlist> list = new LinkedList<>(playlistCache.values());
        Collections.sort(list);
        return list;
    }
    
    public Playlist getPlaylist(String id) {
        Playlist playlist = playlistCache.get(id);
        if (needsFetching(playlist)) {
            playlist = fetchPlaylist(id);
        }

        return playlist;
    }

    public URI getSongUri(Song song) {
        WebTarget wt = buildRequestUri("stream")
                .queryParam("id", song.getId())
                .queryParam("format", "mp3");
        byte[] bytes = wt.request().get(byte[].class);
        System.out.println(bytes.length);
        return wt.getUri();
    }

    //**************************************************************************
    private WebTarget buildRequestUri(String endpoint) {
        return client.target(url + "/rest/" + endpoint + ".view")
                .queryParam("u", user)
                .queryParam("p", pass)
                .queryParam("v", version)
                .queryParam("c", "subsonicClient")
                .queryParam("f", "json");
    }

    private void fetchAllArtists() {
        try {
            JSONObject json = getSubsonicResponse(buildRequestUri("getArtists"));

            JSONArray indexArray = json.getJSONObject("artists").getJSONArray("index");
            for (int i = 0; i < indexArray.length(); i++) {
                JSONObject index = indexArray.getJSONObject(i);
                if (index.optJSONArray("artist") != null) {
                    JSONArray artistArray = index.getJSONArray("artist");
                    for (int artistLoop = 0; artistLoop < artistArray.length(); artistLoop++) {
                        JSONObject obj = artistArray.getJSONObject(artistLoop);
                        Artist artist = new Artist(obj.getString("id"), obj.getString("name"));
                        artistCache.put(artist.getName(), artist);
                    }
                } else {
                    JSONObject obj = index.optJSONObject("artist");
                    if (obj != null) {
                        Artist artist = new Artist(obj.getString("id"), obj.getString("name"));
                        artistCache.put(artist.getName(), artist);
                    }
                }
            }
        } catch (JSONException je) {
            throw new RuntimeException(je.getLocalizedMessage());
        }
    }

    private void fetchAllPlaylists() {
        try {
            JSONObject json = getSubsonicResponse(buildRequestUri("getPlaylists"));

            JSONArray indexArray = json.getJSONObject("playlists").optJSONArray("playlist");
            if (indexArray != null) {
                for (int i = 0; i < indexArray.length(); i++) {
                    JSONObject obj = indexArray.getJSONObject(i);
                    if (obj != null) {
                        Playlist pl = json2Playlist(obj);
                        playlistCache.put(pl.getName(), pl);
                    }
                }
            } else {
                throw new RuntimeException("handle single/no playlist");
            }
        } catch (JSONException je) {
            throw new RuntimeException(je.getLocalizedMessage());
        }
        fetchedPlaylists = true;
    }

    private Playlist fetchPlaylist(String id) {
        Playlist playlist = null;
        try {
            JSONObject json = getSubsonicResponse(buildRequestUri("getPlaylist")
                    .queryParam("id", id));
            JSONObject obj = json.getJSONObject("playlist");
            playlist = json2Playlist(obj);
            JSONArray entryArray = obj.optJSONArray("entry");
            List<Song> songs = new LinkedList<>();
            if (entryArray != null) {
                for (int i = 0; i < entryArray.length(); i++) {
                    songs.add(json2Song(entryArray.getJSONObject(i)));
                }
            } else {
                JSONObject entry = obj.optJSONObject("entry");
                if (entry != null) {
                    songs.add(json2Song(entry));
                }
            }
            Collections.sort(songs);
            playlist.setSongs(songs);
        } catch (JSONException e) {
            
        }
        
        return playlist;
    }
    private Artist fetchArtist(String id) {
        Artist artist = null;
        try {
            JSONObject json
                    = getSubsonicResponse(buildRequestUri("getArtist").queryParam("id", id));
            JSONObject obj = json.getJSONObject("artist");
            artist = json2Artist(obj);
            artist.setAlbums(fetchArtistAlbums(obj));
            artist.setFetched();
            artistCache.put(artist.getId(), artist);
        } catch (JSONException je) {
            throw new RuntimeException(je.getLocalizedMessage());
        }

        return artist;
    }

    protected List<Album> fetchArtistAlbums(JSONObject json) throws JSONException {
        List<Album> albums = null;
        JSONArray array = json.optJSONArray("album");
        if (array != null) {
            albums = new LinkedList<>();
            for (int j = 0; j < array.length(); j++) {
                albums.add(getAlbum(array.getJSONObject(j).getString("id")));
            }
            Collections.sort(albums);
        } else {
            JSONObject obj = json.optJSONObject("album");
            if (obj != null) {
                albums = new LinkedList<>();
                albums.add(getAlbum(obj.getString("id")));
            }
        }
        return albums;
    }

    private boolean needsFetching(LazyModel model) {
        return (model == null) || !model.hasBeenFetched();
    }

    private JSONObject getSubsonicResponse(WebTarget wt) throws JSONException {
        String response = wt.request(MediaType.APPLICATION_JSON).get(String.class);
        JSONObject json = new JSONObject(response).getJSONObject("subsonic-response");
        // Check Subsonic response
        final String status = json.getString("status");
        if (!"ok".equals(status)) {
            throw new RuntimeException(status);
        }

        return json;
    }

    private Album json2Album(JSONObject obj) {
        Album album = new Album();
        try {
            album.setId(obj.getString("id"));
            album.setName(obj.optString("name"));
            album.setArtistId(obj.getString("artistId"));
            album.setArtistName(obj.optString("artist"));
            album.setYear(obj.optInt("year", -1));
        } catch (JSONException je) {
            throw new RuntimeException(je.getLocalizedMessage());
        }

        return album;
    }

    private Artist json2Artist(JSONObject obj) {
        Artist artist = new Artist();

        try {
            artist.setId(obj.getString("id"));
            artist.setName(obj.getString("name"));
            artist.setGenre(obj.optString("genre"));
            artist.setAlbumCount(obj.optInt("albumCount", -1));
        } catch (JSONException je) {
            throw new RuntimeException(je.getLocalizedMessage());
        }

        return artist;
    }

    private Song json2Song(JSONObject obj) {
        Song song = new Song();
        try {
            song.setId(obj.getString("id"));
            song.setAlbumId(obj.optString("albumId"));
            song.setTrack(obj.optInt("track", -1));
            song.setName(obj.getString("title"));
            song.setBitRate(obj.optInt("bitRate", -1));
            song.setDuration(obj.optInt("duration", -1));
            song.setFetched();
        } catch (JSONException je) {
            throw new RuntimeException(je.getLocalizedMessage());
        }
        return song;
    }

    protected Playlist json2Playlist(JSONObject obj) throws JSONException {
        Playlist pl = new Playlist();
        pl.setId(obj.getString("id"));
        pl.setName(obj.getString("name"));
        pl.setOwner(obj.optString("owner"));
        pl.setPublic(obj.optBoolean("public", false));
        pl.setDuration(obj.optLong("duration", -1));
        return pl;
    }

    private static class DefaultTrustManager implements X509TrustManager {

        @Override
        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }

    private static class NoopHostnameVerifier implements HostnameVerifier {

        @Override
        public boolean verify(String string, SSLSession ssls) {
            return true;
        }
    };
}
