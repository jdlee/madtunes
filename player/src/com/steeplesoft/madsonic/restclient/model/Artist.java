/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.madsonic.restclient.model;

import java.util.List;

/**
 *
 * @author jdlee
 */
public class Artist extends LazyModel {
    private String genre;
    private String coverArt;
    private int albumCount;
    private List<Album> albums;
    
    public Artist() {
        
    }
    
    public Artist (String id, String name) {
        super.setId(id);
        super.setName(name);
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getCoverArt() {
        return coverArt;
    }

    public void setCoverArt(String coverArt) {
        this.coverArt = coverArt;
    }

    public int getAlbumCount() {
        return albumCount;
    }

    public void setAlbumCount(int albumCount) {
        this.albumCount = albumCount;
    }

    public List<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(List<Album> albums) {
        this.albums = albums;
    }

    @Override
    public String toString() {
        return "Artist{" + "id=" + id + ", name=" + name + ", genre=" + genre + ", coverArt=" + coverArt + ", albumCount=" + albumCount + ", albums=" + albums + '}';
    }
}
