package com.steeplesoft.madsonic.restclient.model;

/**
 *
 * @author jdlee
 */
public class Song extends LazyModel {
    private String albumId;
    private int track;
    private int duration;
    private int year;
    private int bitRate;
    private String coverArt;
    
    public String getAlbumId() {
        return albumId;
    }

    public void setAlbumId(String albumId) {
        this.albumId = albumId;
    }

    public int getTrack() {
        return track;
    }

    public void setTrack(int track) {
        this.track = track;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getBitRate() {
        return bitRate;
    }

    public void setBitRate(int bitRate) {
        this.bitRate = bitRate;
    }

    public String getCoverArt() {
        return coverArt;
    }

    public void setCoverArt(String coverArt) {
        this.coverArt = coverArt;
    }

    @Override
    public String toString() {
        return "Song{" + "id=" + id + ", albumId=" + albumId + ", track=" + track + ", title=" + name + ", duration=" + duration + ", year=" + year + ", bitRate=" + bitRate + ", coverArt=" + coverArt + '}';
    }
}
