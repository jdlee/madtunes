/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.steeplesoft.madsonic.restclient.model;

/**
 *
 * @author jdlee
 */
public class LazyModel implements Comparable<LazyModel> {
    protected String id;
    protected String name;
    protected boolean fetched = false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean hasBeenFetched() {
        return fetched;
    }
    
    public void setFetched() {
        fetched = true;
    }

    @Override
    public int compareTo(LazyModel other) {
        String thisName = name.toLowerCase();
        String thatName = other.name.toLowerCase();
        
        if (thisName.startsWith("the ")) {
            thisName = thisName.substring(4);
        }
        
        if (thatName.startsWith("the ")) {
            thatName = thatName.substring(4);
        }
        
        return thisName.compareTo(thatName);
    }
}
