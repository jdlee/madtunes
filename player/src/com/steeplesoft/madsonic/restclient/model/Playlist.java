/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.madsonic.restclient.model;

import java.util.List;

/**
 *
 * @author jdlee
 */
public class Playlist extends LazyModel {
    private Long duration;
    private Long songCount;
    private String owner;
    private boolean publicList;
    private String comment;
    private List<Song> songs;

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Long getSongCount() {
        return songCount;
    }

    public void setSongCount(Long songCount) {
        this.songCount = songCount;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public boolean isPublic() {
        return publicList;
    }

    public void setPublic(boolean publicList) {
        this.publicList = publicList;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<Song> getSongs() {
        return songs;
    }

    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }

    @Override
    public String toString() {
        return "Playlist{" + "id=" + id + ", duration=" + duration + ", songCount=" + songCount + ", name=" + name + ", owner=" + owner + ", publicList=" + publicList + ", comment=" + comment + '}';
    }
}
