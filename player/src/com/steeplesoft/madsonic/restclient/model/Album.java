/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.madsonic.restclient.model;

import java.util.List;

/**
 *
 * @author jdlee
 */
public class Album extends LazyModel {
    private String artistId;
    private String artistName;
    private int year;
    private String coverArt;
    private int songCount;
    private List<Song> songs;

//    public Album(JSONObject obj) {
//        super(obj.getString("id"), obj.optString("name"));
//        try {
//            this.artistId = obj.getString("artistId");
//            this.artistName = obj.optString("artist");
//            this.year = obj.optInt("year", -1);
//            this.songCount = obj.optInt("songCount", -1);
//        } catch (JSONException ex) {
//            Logger.getLogger(Album.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//    public Album(String id, String name, String artistId, String artistName, String coverArt) {
//        super(id, name);
//        this.artistId = artistId;
//        this.artistName = artistName;
//        this.coverArt = coverArt;
//    }

    public String getArtistId() {
        return artistId;
    }

    public void setArtistId(String artistId) {
        this.artistId = artistId;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getCoverArt() {
        return coverArt;
    }

    public void setCoverArt(String coverArt) {
        this.coverArt = coverArt;
    }

    public List<Song> getSongs() {
        return songs;
    }

    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }

    @Override
    public String toString() {
        return "Album{" + "id=" + id + ", name=" + name + ", artistId=" + artistId + 
                ", artistName=" + artistName + ", year=" + year + ", coverArt=" + 
                coverArt + ", songCount=" + songCount + ", songs=" + songs + '}';
    }
}
